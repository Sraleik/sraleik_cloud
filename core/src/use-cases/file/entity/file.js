const uuid = require('uuid/v4');
const CID = require('ipfs-only-hash');

function getCidOfData(dataBuffer){
	return dataBuffer ? CID.of(dataBuffer) : Promise.resolve('QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH');
}

class File {
	constructor({ name='newFile', parentFolderId = '/', isEncrypted = false, dataBuffer, metaData } = {}){
		return (async () => {
			if(metaData){
				this.id = metaData.id;
				this.dataBuffer = undefined;
				this.meta = new FileMeta({metaData});
			} else {
				this.id = uuid();
				this.dataBuffer = dataBuffer;
				this.meta = new FileMeta({
					id: this.id, 
					name, 
					isEncrypted,
					cid: await getCidOfData(dataBuffer), 
					parentFolderId
				});
			}

			return this;
		})();
	}

	get name() {
		return this.meta.name;
	}

	get type() {
		return this.meta.type;
	}
        
	get parentFolderId () {
		return this.meta.parentFolderId;
	} 
        
	get cid () {
		return this.meta.cid;
	} 

	get isEncrypted () {
		return this.meta.isEncrypted;
	} 
        
	get createdAt () {
		return this.meta.createdAt;
	} 
        
	get updatedAt () {
		return this.meta.updatedAt;
	} 
}
class FileMeta {
	constructor({id, name = 'newFile', cid, isEncrypted, parentFolderId, metaData}){
		if(metaData){
			this.id = metaData.id; 
			this.type = metaData.type; 
			this.versions = metaData.versions.map((version) => {
				return {
					...version,
					date: new Date(version.date)
				};
			}); 
		}
		else{
			this.id = id;
			this.type = 'file';
			this.versions = [
				{
					id: uuid(),
					date: new Date(),
					cid,
					isEncrypted,
					name,
					parentFolderId
				}
			];
		}
	}

	lastVersionIndex () {
		return this.versions.length - 1;
	}

	get name () {
		return this.versions[this.lastVersionIndex()].name;
	} 
        
	get parentFolderId () {
		return this.versions[this.lastVersionIndex()].parentFolderId;
	} 
        
	get cid () {
		return this.versions[this.lastVersionIndex()].cid;
	} 

	get isEncrypted () {
		return this.versions[this.lastVersionIndex()].isEncrypted;
	} 
        
	get createdAt () {
		return this.versions[0].date;
	} 
        
	get updatedAt () {
		return this.versions[this.lastVersionIndex()].date;
	} 
}

module.exports = { File, FileMeta, getCidOfData };