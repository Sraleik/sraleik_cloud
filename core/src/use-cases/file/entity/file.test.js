const expect = require('chai').expect;
const uuid = require('uuid/v4');
const fs = require('fs');
const { File } = require('./file.js');

describe('Entity: File', () => {
	it('Newly created file should have proper structure', async () => {
		const newFile = await new File();

		expect(newFile.name).to.be.equal('newFile');
		expect(newFile.isEncrypted).to.be.equal(false);
		expect(newFile.type).to.be.equal('file');
		expect(newFile.meta.versions.length).to.be.equal(1);
		expect(newFile.parentFolderId).to.be.equal('/');
		expect(newFile.cid).to.be.equal('QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH');
		expect(newFile.createdAt).to.be.a('Date');
		expect(newFile.updatedAt).to.be.a('Date');
		expect(newFile.updatedAt).to.be.equal(newFile.createdAt);
	});

	it('Newly created file with name should have the proper name', async () => {
		const newFile = await new File({ name: 'mass-effect.jpg' });

		expect(newFile.name).to.be.equal('mass-effect.jpg');
	});
	it('Newly created file with file data should have proper CID', async () => {
		const dataBuffer = fs.readFileSync(__dirname+'/mass_effect.jpg');
		const newFile = await new File({ name: 'mass-effect.jpg', dataBuffer });
		expect(newFile.cid).to.be.equal('QmWKkkeJnRvKVneS195GyeGSj4ihBFunVQKMvKPZZwLokm');
	});

	it('File can be create with metaData', async () => {
		const fileMetaData = {
			id: uuid(),
			type:'file',
			versions: [
				{
					date: new Date().toISOString(),
					cid: 'QmbFMke1KXqnYyBBWxB74N4c5SBnJMVAiMNRcGu6x1AwQH',
					name: 'newFile',
					isEncrypted: false,
					parentFolderId: '/' 
				}
			]
		};

		const newFile = await new File({ metaData: fileMetaData });
		expect(JSON.stringify(newFile.meta)).to.be.equal(JSON.stringify(fileMetaData));
		expect(newFile.meta.versions[0].date).to.be.a('Date');
	});
});
