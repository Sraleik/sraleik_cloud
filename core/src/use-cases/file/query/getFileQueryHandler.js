const { File } = require('../entity/file.js');
class GetFileQueryHandler {
	constructor(metaStorageConnection) {
		this.metaStorageConnection = metaStorageConnection;
	}
    
	async handle(query) {
		const fileMetaData = 
			await this.metaStorageConnection
				.read(query)
				.catch((e) => console.log(e));
				
		const file = await new File({
			metaData: fileMetaData
		});
        
		return file;
	}
}

module.exports = { GetFileQueryHandler };