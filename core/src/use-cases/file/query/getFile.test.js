const expect = require('chai').expect;

const { createMockBluzelle } = require('@sraleik/mock-bluzelle');
const { createBluzelleMetaStorage: createMetaStorage } = require('../../../../../meta-storage/lib/index.js');

const { File } = require('../entity/file.js');

const { GetFileQueryHandler } = require('./getFileQueryHandler.js');

describe('Query: File', () => {
	it('Get File', async () => {
		const mockBluzelle = await createMockBluzelle();
		const store = await createMetaStorage(mockBluzelle);

		const newFile = await new File({name: 'superImg.jpg'});
		await store.set(newFile.meta);


		const getFileIntent = {queryName: 'getFile', id: newFile.id, type: newFile.type};
		const getFileHandler = new GetFileQueryHandler(store);

		const retrievedFile = await getFileHandler.handle(getFileIntent);

		expect(retrievedFile).to.be.deep.equal(newFile);
	});
});
