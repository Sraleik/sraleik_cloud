const expect = require('chai').expect;
const fs = require('fs');
const { createMockBluzelle } = require('@sraleik/mock-bluzelle');
const { createBluzelleMetaStorage: createMetaStorage } = require('../../../../../meta-storage/lib/index.js');

const { File } = require('../entity/file.js');
const { FileRepository }= require('./fileRepository.js');

describe('Repository: File', () => {
	it('Save a file with no data', async () => {
		const mockBluzelle = await createMockBluzelle();
		const store = await createMetaStorage(mockBluzelle);

		const newFile = await new File({name:'repoTest.txt'});

		const fileRepository = new FileRepository(store);
		await fileRepository.add(newFile);

		const retrievedFileData = await store.read({id: newFile.meta.id, type: newFile.meta.type}); 
		const retrievedFile = await new File({metaData: retrievedFileData});

		expect(retrievedFile).to.be.deep.equal(newFile);
	}); 

	it('Save a file with data', async () => {
		const mockBluzelle = await createMockBluzelle();
		const store = await createMetaStorage(mockBluzelle);
		const ipfsMock = { add: (dataBuffer) => Promise.resolve(dataBuffer) };

		const dataBuffer = fs.readFileSync(__dirname + '/cute_cat.jpg');

		const newFile = await new File({name:'cute_cat.jpg', dataBuffer});

		const fileRepository = new FileRepository(store, ipfsMock);
		await fileRepository.add(newFile);

		const retrievedFileData = await store.read({id: newFile.meta.id, type: newFile.meta.type}); 
		const retrievedFile = await new File({metaData: retrievedFileData});
		newFile.dataBuffer = undefined;

		expect(retrievedFile).to.be.deep.equal(newFile);
	}); 
});
