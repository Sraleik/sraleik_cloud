class FileRepository {
	constructor(metaStorageConnection, dataStorageConnection){
		this.metaStorageConnection = metaStorageConnection;
		this.dataStorageConnection = dataStorageConnection;
	}

	async add(fileEntity) {
		await this.metaStorageConnection.set(fileEntity.meta).catch((err) => {
			throw new Error(err);
		}); 

		if(fileEntity.dataBuffer) {
			await this.dataStorageConnection.add(fileEntity.dataBuffer).catch((err) => console.log(err));
		}
	}
}

module.exports = { FileRepository };