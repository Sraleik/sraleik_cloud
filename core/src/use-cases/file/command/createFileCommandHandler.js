const { File } = require('../entity/file.js');
class CreateFileCommandHandler {
	constructor(repository) {
		this.repository = repository;
	}
    
	async handle(command) {
		const file = await new File({
			name: command.fileName,
			dataBuffer: command.dataBuffer,
			parentFolderId: command.destinationFolderId,
			isEncrypted: Boolean(command.encryptionKey)
		});
        
		await this.repository.add(file);
		return file.id;
	}
}
module.exports = { CreateFileCommandHandler }; 