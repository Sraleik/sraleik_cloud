class CreateFileCommand {
	constructor({fileName, dataBuffer, destinationFolderId, encryptionKey} = {}){
		this.commandName = 'createFile', 
		this.fileName = fileName;
		this.dataBuffer = dataBuffer;
		this.destinationFolderId = destinationFolderId;
		this.encryptionKey = encryptionKey; 
	}
}

module.exports = { CreateFileCommand };