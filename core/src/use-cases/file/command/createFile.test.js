const expect = require('chai').expect;
const chai = require('chai');
chai.use(require('chai-uuid'));

const { createMockBluzelle } = require('@sraleik/mock-bluzelle');
const { createBluzelleMetaStorage: createMetaStorage } = require('../../../../../meta-storage/lib/index.js');

const { FileRepository } = require('../repository/fileRepository.js');
const { CreateFileCommand } = require('./createFileCommand.js');
const { CreateFileCommandHandler } = require('./createFileCommandHandler.js');

describe('Command: Create File', () => {
	it('Create default File', async () => {
		const mockBluzelle = await createMockBluzelle();
		const store = await createMetaStorage(mockBluzelle);


		const fileRepository = new FileRepository(store);

		const createFileIntent = new CreateFileCommand();
		const createFileHandler = new CreateFileCommandHandler(fileRepository);

		const newFileId = await createFileHandler.handle(createFileIntent);

		expect(newFileId).to.be.a.uuid('v4');
	});
});