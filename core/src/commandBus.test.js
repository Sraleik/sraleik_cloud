const expect = require('chai').expect;
const chai = require('chai');
chai.use(require('chai-uuid'));

const { mysqlConnection } = require('../../meta-storage/lib/index.js');
const { createMysqlMetaStorage: createMetaStorage } = require('../../meta-storage/lib/index.js');
// const { createMockBluzelle } = require('@sraleik/mock-bluzelle');
// const { createBluzelleMetaStorage: createMetaStorage } = require('../../meta-storage/lib/index.js');

const { PrivateKey } = require('eciesjs');

const { CreateFileCommand } = require('./use-cases/file/command/createFileCommand');

const { CommandBus } = require('./commandBus');
let databaseConnection, metaStore;

describe('Bus: Command', () => {
	before(async function() {
		// const databaseConnection = await createMockBluzelle();
		databaseConnection = await mysqlConnection;
		metaStore = await createMetaStorage(databaseConnection);
	});

	after(async function() {
		await metaStore.close(); 
	});

	it('Create public file', async () => {
		const ipfsMock = { add: (dataBuffer) => Promise.resolve(dataBuffer) };

		const createFileIntent = new CreateFileCommand({
			fileName: 'busTest.txt', 
			dataBuffer: Buffer.from('testBusNotEncrypted')
		});
		
		const bus = new CommandBus({metaStore, dataStore: ipfsMock}); 
		const retrievedFileId = await bus.handle(createFileIntent);

		expect(retrievedFileId).to.be.a.uuid('v4');
	});

	it('Create private file', async () => {
		const ipfsMock = { add: (dataBuffer) => Promise.resolve(dataBuffer) };

		const privateKey = 'a0cfd7968b24bf352b350234a1cd3cc28ad0d5ceedf8cf62075e1f39ce245737';  	 
		const keyPair = PrivateKey.fromHex(privateKey);

		const createFileIntent = new CreateFileCommand({
			fileName: 'busTest.txt', 
			dataBuffer: Buffer.from('testBusEncrypted'),
			encryptionKey: keyPair.publicKey.toHex() 
		});

		const bus = new CommandBus({metaStore, dataStore: ipfsMock}); 
		const retrievedFileId = await bus.handle(createFileIntent);

		expect(retrievedFileId).to.be.a.uuid('v4');
	});
});
