const log4js = require('log4js');
const defaultLogger = log4js.getLogger();
// defaultLogger.level = 'debug';

class TimingMiddleware {

	constructor({ logger = defaultLogger, next }){
		this.logger = logger;
		this.next = next;
	}

	async handle(command){
		const startTime = Date.now();
		const response = await this.next.handle(command);
		const endTime = Date.now();
		const elapsed = endTime - startTime;
		const status = command.encryptionKey ? 'Encrypted': 'Not Encrypted';
		const message = `Command ${command.commandName} (${status}) took ${elapsed} ms`;
		this.logger.info(message);
		return response;
	}

}

module.exports = { TimingMiddleware };