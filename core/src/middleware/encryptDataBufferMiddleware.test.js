const expect = require('chai').expect;

const { decrypt, PrivateKey } = require('eciesjs');

const { EncryptDataBufferMiddleware } = require('./encryptDataBufferMiddleware');


describe('Middleware: Encrypt DataBuffer', () => {
	it('Should encrypt command data and call next()', async () => {
		// this will return the command modified by the encrypt middleware to this test
		const nextMock = {
			handle: (command) => {
				return Promise.resolve(command);
			}
		};

		const privateKey = 'a0cfd7968b24bf352b350234a1cd3cc28ad0d5ceedf8cf62075e1f39ce245737';

		const keyPair = PrivateKey.fromHex(privateKey);
		const dataBuffer = new Buffer.from('middlewareTest');

		const createFileCommandMock = {
			dataBuffer,
			encryptionKey: keyPair.publicKey.toHex()
		};

		const encriptFileDataMiddleware = new EncryptDataBufferMiddleware({ next: nextMock });

		const createFileCommandEncrypted = await encriptFileDataMiddleware.handle(createFileCommandMock);

		const decryptDataBuffer = decrypt(keyPair.toHex(), createFileCommandEncrypted.dataBuffer);

		// check that the data is encrypted
		expect(createFileCommandEncrypted.dataBuffer).to.not.be.equal(dataBuffer);

		// check that the data is properly encrypted by verifing its decrypted content 
		expect(decryptDataBuffer.toString()).to.be.equal(dataBuffer.toString());
	}); 
});
