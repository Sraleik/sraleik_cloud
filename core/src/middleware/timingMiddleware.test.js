const expect = require('chai').expect;

const { TimingMiddleware } = require('./timingMiddleware');

class LoggerMock {
	constructor() {
		this.messages = [];
	}

	info(message) {
		this.messages.push(message);
	}
}

describe('Middleware: Logger', () => {
	it('Should take 250ms~', async () => {
		//this will simulate a command that takes 250ms to complete
		const nextMock = {
			handle: (command) => {
				return new Promise((resolve) => {
					setTimeout(function() {
						resolve(command); 
					}, 250); 
				});
			}
		};

		const loggerMock = new LoggerMock;

		const createMockCommand = {commandName: 'dummyCommand'};

		const timmingMiddleware = new TimingMiddleware({ logger: loggerMock, next: nextMock });

		await timmingMiddleware.handle(createMockCommand);

		const lastLogMessage = loggerMock.messages[0];
		const executionTime = parseInt(lastLogMessage.match(/\d+/)); 

		expect(executionTime).to.be.greaterThan(249);
		expect(executionTime).to.be.lessThan(400);
	}); 
});
