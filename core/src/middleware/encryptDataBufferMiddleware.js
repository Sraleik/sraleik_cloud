const { encrypt: defaultEncrypt} = require('eciesjs');

class EncryptDataBufferMiddleware {

	constructor({ encrypt = defaultEncrypt, next }){
		this.encrypt = encrypt;
		this.next = next;
	}

	async handle(command){
		if(command.encryptionKey && command.dataBuffer) {
			command.dataBuffer = this.encrypt(command.encryptionKey, command.dataBuffer);
		}
		
		return await this.next.handle(command);
	}
}

module.exports = { EncryptDataBufferMiddleware };