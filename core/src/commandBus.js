const { FileRepository } = require('./use-cases/file/repository/fileRepository');
const { CreateFileCommandHandler } = require('./use-cases/file/command/createFileCommandHandler');
const { EncryptDataBufferMiddleware } = require('./middleware/encryptDataBufferMiddleware');
const { TimingMiddleware } = require('./middleware/timingMiddleware');

class CommandBus {
	constructor({metaStore, dataStore }){
		this.metaStore = metaStore;
		this.dataStore = dataStore;
	}

	handle(command){
		if(command.commandName === 'createFile'){
			const fileRepository = new FileRepository(this.metaStore, this.dataStore);
			const createFileHandler = new CreateFileCommandHandler(fileRepository);
			const encriptDataBufferMiddleware = new EncryptDataBufferMiddleware({next: createFileHandler});
			const timingMiddleware = new TimingMiddleware({next: encriptDataBufferMiddleware});

			return timingMiddleware.handle(command);
		}
	}
}


module.exports = { CommandBus };