const ipfsMock = { add: (dataBuffer) => Promise.resolve(dataBuffer) };
const { createMockBluzelle } = require('@sraleik/mock-bluzelle');
const { createBluzelleMetaStorage, createMysqlMetaStorage, mysqlConnection } = require('../meta-storage/lib/index.js');

const { CommandBus } = require('./src/commandBus');


async function getMysqlBus(){
	const databaseConnection = await mysqlConnection;
	const metaStore = await createMysqlMetaStorage(databaseConnection);

	return  new CommandBus({ metaStore, dataStore: ipfsMock }); 
}

async function getBluzelleBus(){
	const databaseConnection = await createMockBluzelle();
	const metaStore = await createBluzelleMetaStorage(databaseConnection);

	return  new CommandBus({ metaStore, dataStore: ipfsMock }); 
}


module.exports = { getMysqlBus, getBluzelleBus };
