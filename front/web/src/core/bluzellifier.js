/* eslint-disable prefer-promise-reject-errors */
export default function createBluzelleLikeBdd (bdd) {
  function create (key, value) {
    return new Promise((resolve, reject) => {
      if (bdd.getItem(key)) {
        reject(new Error(`The key: ${key} already exist`))
      } else {
        resolve(
          bdd.setItem(key, value)
        )
      }
    })
  }

  function update (key, value) {
    return new Promise((resolve, reject) => {
      if (bdd.getItem(key)) {
        resolve(
          bdd.setItem(key, value)
        )
      } else {
        reject(new Error(`The key: ${key} doesn't exist`))
      }
    })
  }

  function read (key) {
    return new Promise((resolve, reject) => {
      resolve(
        bdd.getItem(key)
      )
    })
  }

  function remove (key) {
    return new Promise((resolve, reject) => {
      resolve(
        bdd.removeItem(key)
      )
    })
  }

  function has (key) {
    return new Promise((resolve, reject) => {
      resolve(
        Boolean(bdd.getItem(key))
      )
    })
  }

  return function bddConnection (keyPair) {
    return new Promise((resolve, reject) => {
      resolve({
        create,
        update,
        read,
        quickread: read,
        delete: remove,
        has
      })
    })
  }
}
