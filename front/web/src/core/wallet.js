const bip39 = require('bip39')
const sha256 = require('js-sha256').sha256
const { PrivateKey } = require('eciesjs')

export function createSeedPhrase () {
  return bip39.generateMnemonic()
}

export function validateSeedPhrase (seedPhrase) {
  return bip39.validateMnemonic(seedPhrase)
}

export function getSeedHash (seedPhrase) {
  return sha256(seedPhrase)
}

export function getKeyPairFromSeedHash (seedHash) {
  return PrivateKey.fromHex(seedHash)
}

export function getKeyPair (seedPhrase) {
  const seedHash = getSeedHash(seedPhrase)
  return getKeyPairFromSeedHash(seedHash)
}
