import uuid from 'uuid/v4'
import { cloneDeep } from 'lodash-es'

function createVersion ({ name, ipfsId, parentFolder }) {
  return {
    date: new Date(),
    ipfsId: ipfsId,
    name: name,
    parentFolder
  }
}

function makeItemFromData (itemData) {
  const itemLastVersion = itemData.versions[itemData.versions.length - 1]

  return {
    ...itemData,
    name: itemLastVersion.name,
    ipfsId: itemLastVersion.ipfsId,
    parentFolder: itemLastVersion.parentFolder,
    createdAt: itemData.versions[0].date,
    updatedAt: itemData.versions[itemData.versions.length - 1].date
  }
}

function createItem ({
  type,
  name,
  ipfsId,
  parentFolder = 'root'
}) {
  const itemData = {
    id: uuid(),
    type: type,
    versions: [
      createVersion({
        name,
        ipfsId,
        parentFolder
      })
    ]
  }

  return makeItemFromData(itemData)
}

function createFile ({
  name = 'New file',
  ipfsId,
  parentFolder
} = {}) {
  if (!ipfsId) {
    throw new Error("Can't create a file with no ipfsId")
  }
  return createItem({ name, type: 'file', ipfsId, parentFolder })
}

function createFolder ({
  name = 'New folder',
  ipfsId = 'QmcsjhuP5XYSfmh51E8UDhjJb6MeuT9hv8cNyWctjsjncK',
  parentFolder
} = {}) {
  return createItem({ name, type: 'folder', ipfsId, parentFolder })
}

function move ({ item, folderId }) {
  const itemData = cloneDeep(item)
  const newVersionParams = {
    ...itemData.versions[itemData.versions.length - 1],
    parentFolder: folderId
  }
  itemData.versions.push(createVersion(newVersionParams))

  return makeItemFromData(itemData)
}

function remove ({ item }) {
  const itemData = cloneDeep(item)

  itemData.deletedAt = new Date()

  return makeItemFromData(itemData)
}

function rename ({ item, name }) {
  const itemData = cloneDeep(item)
  const newVersionParams = {
    ...itemData.versions[itemData.versions.length - 1],
    name
  }
  itemData.versions.push(createVersion(newVersionParams))

  return makeItemFromData(itemData)
}

export default {
  createFolder,
  createFile,
  rename,
  delete: remove,
  move
}
