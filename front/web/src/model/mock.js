const simpleFolder = {
  id: 'fab99260-783c-46bc-a225-897386a75986',
  type: 'folder',
  versions: [
    {
      date: new Date('2020-01-17T22:03:15.004Z'),
      name: '/',
      children: []
    },
    {
      date: new Date('2020-01-17T22:03:15.004Z'),
      name: '/',
      children: ['d40cc68c-33d4-4cb5-87c5-5bce8178bc2e']
    }
  ],
  name: '/',
  createdAt: new Date('2020-01-17T22:03:15.004Z'),
  updatedAt: new Date('2020-01-17T22:03:15.004Z')
}

const simpleFile = {
  id: 'd40cc68c-33d4-4cb5-87c5-5bce8178bc2e',
  type: 'file',
  versions: [
    {
      id: 'f3b4f0be-5f64-4261-a028-379bd8543258',
      date: new Date('2020-01-17T22:03:15.004Z'),
      cid: 'QmaLcm7jZwHK5KpvBUvWJ9prqbbhsDXMXbwarNc3ssLVR8',
      name: 'mass-effect.jpg',
      parentFolder: 'fab99260-783c-46bc-a225-897386a75986'
    }
  ],
  name: 'mass-effect.jpg',
  createdAt: new Date('2020-01-17T22:03:15.004Z'),
  updatedAt: new Date('2020-01-17T22:03:15.004Z'),
  cid: 'QmaLcm7jZwHK5KpvBUvWJ9prqbbhsDXMXbwarNc3ssLVR8',
  parentFolder: 'fab99260-783c-46bc-a225-897386a75986'
}

const simpleFileDataStore = {
  id: 'd40cc68c-33d4-4cb5-87c5-5bce8178bc2e',
  type: 'file',
  versions: [
    {
      id: 'f3b4f0be-5f64-4261-a028-379bd8543258',
      date: new Date('2020-01-17T22:03:15.004Z'),
      cid: 'QmaLcm7jZwHK5KpvBUvWJ9prqbbhsDXMXbwarNc3ssLVR8',
      name: 'mass-effect.jpg',
      parentFolder: 'fab99260-783c-46bc-a225-897386a75986'
    }
  ]
}

export {
  simpleFolder,
  simpleFile,
  simpleFileDataStore
}
