import Vue from 'vue'
import VueRouter from 'vue-router'
import * as Views from '../views/'
import { store } from '@/store/'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Views.Home,
    beforeEnter (to, from, next) {
      if (store.myKeyPair || localStorage.seedHash) {
        next()
      } else {
        next({ name: 'login', query: { ...to.query, ...from.query }, params: to.params })
      }
    }
  },
  {
    path: '/login',
    name: 'login',
    component: Views.Login
  },
  {
    path: '/signup',
    name: 'signup',
    component: Views.Signup
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
