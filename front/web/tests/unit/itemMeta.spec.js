import Item from '@/model/itemMeta'
import { simpleFolder } from '@/model/mock'
const expect = require('chai').expect

describe('Item', () => {
  describe('Create', () => {
    describe('File', () => {
      it('Should create a file with default name', async () => {
        const file = Item.createFile({ ipfsId: 'QmWKkkeJnRvKVneS195GyeGSj4ihBFunVQKMvKPZZwLokm' })

        expect(file.name).to.be.equal('New file')
        expect(file.type).to.be.equal('file')
        expect(file.parentFolder).to.be.equal('root')
        expect(file.ipfsId).to.be.equal('QmWKkkeJnRvKVneS195GyeGSj4ihBFunVQKMvKPZZwLokm')
      })

      it('Should create a file with proper name', async () => {
        const file = Item.createFile({ name: 'Mass Effect.jpg', ipfsId: 'QmWKkkeJnRvKVneS195GyeGSj4ihBFunVQKMvKPZZwLokm' })

        expect(file.name).to.be.equal('Mass Effect.jpg')
      })

      it('Should create a file with proper parentFolder', async () => {
        const file = Item.createFile({ parentFolder: 'randomFolder', ipfsId: 'QmWKkkeJnRvKVneS195GyeGSj4ihBFunVQKMvKPZZwLokm' })

        expect(file.parentFolder).to.be.equal('randomFolder')
      })

      it('Should throw an error if no ipfsId is given', async () => {
        const createFileWithNoIpfsId = Item.createFile

        expect(() => createFileWithNoIpfsId()).to.Throw()
      })
    })
    describe('Folder', () => {
      it('Should create an empty folder', async () => {
        const folder = Item.createFolder()

        expect(folder.name).to.be.equal('New folder')
        expect(folder.type).to.be.equal('folder')
        expect(folder.parentFolder).to.be.equal('root')
        expect(folder.ipfsId).to.be.equal('QmcsjhuP5XYSfmh51E8UDhjJb6MeuT9hv8cNyWctjsjncK')
      })

      it('Should create an empty folder with proper name', async () => {
        const folder = Item.createFolder({ name: 'Image' })

        expect(folder.name).to.be.equal('Image')
      })

      it('Should create an empty folder with proper parentFolder', async () => {
        const folder = Item.createFolder({ parentFolder: 'f1aa623b-53fc-4d1c-a2b2-b10b5aad350d' })

        expect(folder.parentFolder).to.be.equal('f1aa623b-53fc-4d1c-a2b2-b10b5aad350d')
      })
    })
  })

  describe('Delete', () => {
    it('Should delete the folder', async () => {
      const folder = simpleFolder
      const newFolder = Item.delete({ item: folder })
      expect(folder).to.not.have.property('deletedAt')

      // eslint-disable-next-line no-unused-expressions
      expect(!!newFolder.deletedAt).to.be.true
    })
  })

  describe('Update', () => {
    it('Should rename the folder', async () => {
      const folder = simpleFolder
      const newFolder = Item.rename({ item: folder, name: 'Video' })

      expect(newFolder.name).to.be.equal('Video')
      expect(newFolder.versions.length).to.be.equal(2)
      expect(newFolder.id).to.be.equal(folder.id)
      expect(newFolder.type).to.be.equal(folder.type)
      expect(newFolder.updatedAt).to.not.be.equal(folder.updatedAt)
    })

    it('Should move the folder', async () => {
      const folder = simpleFolder
      const newFolder = Item.move({ item: folder, folderId: 'anotherFolderId' })

      expect(newFolder.name).to.be.equal(folder.name)
      expect(newFolder.parentFolder).to.be.equal('anotherFolderId')
      expect(newFolder.id).to.be.equal(folder.id)
      expect(newFolder.type).to.be.equal(folder.type)
      expect(newFolder.updatedAt).to.not.be.equal(folder.updatedAt)
    })
  })
})
