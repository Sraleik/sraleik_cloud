sraleikcloud-console
====================

cli application than use sraleik-cloud core feature

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/sraleikcloud-console.svg)](https://npmjs.org/package/sraleikcloud-console)
[![Downloads/week](https://img.shields.io/npm/dw/sraleikcloud-console.svg)](https://npmjs.org/package/sraleikcloud-console)
[![License](https://img.shields.io/npm/l/sraleikcloud-console.svg)](https://github.com/Sraleik/sraleikcloud-console/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g @sraleik/cloud-console
$ sraleik-cloud COMMAND
running command...
$ sraleik-cloud (-v|--version|version)
@sraleik/cloud-console/0.0.1 linux-x64 node-v12.13.1
$ sraleik-cloud --help [COMMAND]
USAGE
  $ sraleik-cloud COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`sraleik-cloud create-file`](#sraleik-cloud-create-file)
* [`sraleik-cloud get-file`](#sraleik-cloud-get-file)
* [`sraleik-cloud hello`](#sraleik-cloud-hello)
* [`sraleik-cloud help [COMMAND]`](#sraleik-cloud-help-command)

## `sraleik-cloud create-file`

Create a file on your system

```
USAGE
  $ sraleik-cloud create-file

OPTIONS
  -n, --name=name  file name

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/create-file.js](https://github.com/Sraleik/cloud-console/blob/v0.0.1/src/commands/create-file.js)_

## `sraleik-cloud get-file`

get a file on your system

```
USAGE
  $ sraleik-cloud get-file

OPTIONS
  -i, --fileId=fileId  file id

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/get-file.js](https://github.com/Sraleik/cloud-console/blob/v0.0.1/src/commands/get-file.js)_

## `sraleik-cloud hello`

Describe the command here

```
USAGE
  $ sraleik-cloud hello

OPTIONS
  -n, --name=name  name to print

DESCRIPTION
  ...
  Extra documentation goes here
```

_See code: [src/commands/hello.js](https://github.com/Sraleik/cloud-console/blob/v0.0.1/src/commands/hello.js)_

## `sraleik-cloud help [COMMAND]`

display help for sraleik-cloud

```
USAGE
  $ sraleik-cloud help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src/commands/help.ts)_
<!-- commandsstop -->
