const { Command, flags } = require('@oclif/command');

const { CreateFileCommand: CreateFileIntent } = require('../../../../core/src/use-cases/file/command/createFileCommand.js');
const { getBluzelleBus } = require('../../../../core/index.js');

class CreateFileCommand extends Command {
	async run() {
		// Create Bus
		const bus = await getBluzelleBus(); 

		const {flags} = this.parse(CreateFileCommand);
		const fileName = flags.name;


		const createFileIntent = new CreateFileIntent({
			fileName
		});
    
		const fileId = await bus.handle(createFileIntent);

		this.log(`File: '${fileName}' created with id: ${fileId}`);
		this.exit(0);
	}
}

CreateFileCommand.description = `Create a file on your system
...
Extra documentation goes here
`;

CreateFileCommand.flags = {
	name: flags.string({char: 'n', description: 'file name'}),
};

module.exports = CreateFileCommand;
