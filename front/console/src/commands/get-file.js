const { Command, flags } = require('@oclif/command');
const { createMockBluzelle } = require('@sraleik/mock-bluzelle');
const { createBluzelleMetaStorage: createMetaStorage } = require('../../../../meta-storage/lib/index.js');
const { GetFileQueryHandler } = require('../../../../core/src/use-cases/file/query/getFileQueryHandler.js');

class GetFileCommand extends Command {
	async run() {
		const databaseConnection = await createMockBluzelle();
		const metaStore = await createMetaStorage(databaseConnection);

		const {flags} = this.parse(GetFileCommand);
		const fileId = flags.fileId;

		const getFileIntent = ({id: fileId, type: 'file'});
		const getFileQueryHandler = new GetFileQueryHandler(metaStore);

		const myFile = await getFileQueryHandler.handle(getFileIntent);

		this.log(myFile.meta);
		this.exit(0);
	}
}

GetFileCommand.description = `get a file on your system
...
Extra documentation goes here
`;

GetFileCommand.flags = {
	fileId: flags.string({char: 'i', description: 'file id'}),
};

module.exports = GetFileCommand;
