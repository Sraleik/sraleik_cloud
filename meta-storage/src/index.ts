import { createBluzelleMetaStorage } from './adapter/bluzelle/bluzelle-adapter';
import { createMysqlMetaStorage } from './adapter/mysql/mysql-adapter';
import { createConnection } from 'typeorm';

import { FileMeta } from './entity/FileMeta';
import { FileVersion } from './entity/FileVersion';


const mysqlConnection = createConnection(
    {
        "type": "mysql",
        "host": "127.0.0.1",
        "port": 3306,
        "username": "root",
        "password": "password",
        "database": "sraleik-cloud",
        "synchronize": true,
        "logging": false,
        "entities": [FileMeta, FileVersion],
    }
)

export { createBluzelleMetaStorage, createMysqlMetaStorage, mysqlConnection };
